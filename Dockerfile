FROM registry.gitlab.com/codelibre/containers/ome-files-build-ubuntu-22.04:latest
MAINTAINER rleigh@codelibre.net

RUN apt-get update \
  && DEBIAN_FRONTEND=noninteractive apt remove -y -qq --no-install-recommends \
    libxerces-c-dev \
    libxerces-c3.2 \
    libxalan-c-dev \
    libxalan-c112 \
    xalan
